import React from "react";
import { Badge } from 'antd';

export function addItemStatus(status) {
    var badge;
    if (status === "in-stock") {
        badge = "processing";
    }
    return(
        <Badge status={badge} text={status} />
    );
}