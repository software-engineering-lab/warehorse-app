export function addKeyInventoryTable(inventory) {
    var inventoryForTable = [];
    for(var building = 0; building < inventory.length; building++) {
        var eachInventory = inventory[building];
        let i = 1;
        for (const key in eachInventory) {
            if (notEqualThis(key)) {
                continue;
            }
            eachInventory[key]["key"] = "" + i;
            inventoryForTable.push(eachInventory[key]);
            i++;
        }
    }
    return inventoryForTable
}

function notEqualThis(key) {
    return (key === "_id" || 
            key === "type" ||
            key === "section" ||
            key === "level" ||
            key === "bay" ||
            key === "shelf");
}
