import axios from "axios";

export function addNewInventory(inventory) {
    return dispatch => {
        return axios.post("http://localhost:8080/inventory", inventory);
    };
}

export function getAllInventory() {
    return dispatch => {
        return axios.get("http://localhost:8080/inventory/all");
    };
}