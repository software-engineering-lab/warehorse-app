export function updatePageName(pageName) {
    return {
        type: "UPDATE_PAGE_NAME",
        pageName
    };
}