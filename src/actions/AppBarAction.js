export function toggleMenuBar(status) {
    return {
        type: "TOGGLE_MENU_BAR",
        status
    };
}