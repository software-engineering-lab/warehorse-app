import axios from "axios";

export function suggestInventoryLocation(data) {
    return dispatch => {
        return axios.post("http://localhost:8080/inventory/find", data);
    };
}

export function saveStockAndLocation(data) {
    return dispatch => {
        return axios.post("http://localhost:8080/stock/", data);
    };
}

export function getAllStock() {
    return dispatch => {
        return axios.get("http://localhost:8080/stock/");
    };
}