import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { toggleMenuBar } from "../../actions/AppBarAction";
import { Layout, Menu, Icon } from "antd";

const { Sider } = Layout;

class AppMenu extends Component {
    constructor(props) {
        super(props);
        this.onCollapse = this.onCollapse.bind(this);
    }

    onCollapse() {
        this.props.dispatch(toggleMenuBar(!this.props.AppBarToggle));
    }

    render() {
        const collapsed = this.props.AppBarToggle;
        return (
            <Sider
                collapsible
                collapsed={collapsed}
                onCollapse={this.onCollapse}
            >
                <div style={{ margin: "16px", textAlign: "center" }}>
                    <Link to="/" style={{ textDecoration: "none" }}>
                        <h1 style={{ color: "rgba(255, 255, 255, 0.8)" }}>{collapsed ? "W" : "Warehorse"}</h1>
                    </Link>
                </div>
                <Menu theme="dark" mode="inline">
                    <Menu.Item key="1">
                        <Link to="/add-stock">
                            <Icon type="plus" />
                            <span>Add Stock</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <Link to="/stocks">
                            <Icon type="search" />
                            <span>See All Stocks</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Link to="/inventory">
                            <Icon type="hdd" />
                            <span>Inventory</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}

function mapStateToProps(state) {
    return {
        AppBarToggle: state.AppBarToggle
    };
}

export default connect(mapStateToProps)(AppMenu);
