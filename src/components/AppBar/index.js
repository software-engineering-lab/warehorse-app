import React, { Component } from "react";
import { connect } from "react-redux";
import { toggleMenuBar } from "../../actions/AppBarAction";
import { Layout, Icon } from "antd";

const { Header } = Layout;

class AppBar extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.props.dispatch(toggleMenuBar(!this.props.AppBarToggle));
    }

    render() {
        return (
            <Header style={{ background: "#fff", padding: 0 }}>
                <Icon
                    className="trigger"
                    onClick={this.toggle}
                    type={this.props.AppBarToggle ? "menu-unfold" : "menu-fold"}
                    style={{
                        fontSize: "18px",
                        lineHeight: "64px",
                        padding: "0 24px",
                        cursor: "pointer",
                        transition: "color .3s"
                    }}
                />
                {this.props.PageName}
            </Header>
        );
    }
}

function mapStateToProps(state) {
    return {
        AppBarToggle: state.AppBarToggle,
        PageName: state.PageName
    };
}

export default connect(mapStateToProps)(AppBar);
