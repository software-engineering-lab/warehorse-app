import React, { Component } from "react";
import { connect } from "react-redux";
import SuggestLocated from "../SuggestLocated/";
import { suggestInventoryLocation } from "../../actions/Stock";
import { Form, Input, Button, Divider, DatePicker, Icon, message, Select, Col, Tooltip, InputNumber } from "antd";

const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;

class AddStockForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submit: false,
            suggestData: {},
            formData: {},
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleGoBack = this.handleGoBack.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.props.suggestInventoryLocation(values).then(res =>  {
                    this.setState({
                        submit: true,
                        suggestData: res.data,
                        formData: values
                    })
                    message.success("Please select location to stock");
                })
            }
        });
    }

    handleGoBack() {
        this.setState({
            submit: false
        })
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
                md: { span: 4 },
                xl: { span: 4 },
                xxl: { span: 2 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
                md: { span: 6 },
                xl: { span: 6 },
                xxl: { span: 4 },
            }
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0
                },
                sm: {
                    span: 24,
                    offset: 2
                }
            }
        };

        return (
            <div>
            {this.state.submit ? (
                    <div>
                        <Button type="primary" onClick={this.handleGoBack}>
                            <Icon type="left" />Go Back
                        </Button>
                        <br /><br />
                        <SuggestLocated suggestData={this.state.suggestData} formData={this.state.formData} />
                    </div>
                ) : 
                (<Form onSubmit={this.handleSubmit}>
                    <h3>Stock Info.</h3>
                    <br />
                    <FormItem {...formItemLayout} label="Seller">
                        {getFieldDecorator("seller", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input seller!"
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Buyer">
                        {getFieldDecorator("buyer", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input buyer!"
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Check In-Out"
                        {...formItemLayout}
                        >
                        <Col span={11}>
                            <FormItem>
                                {getFieldDecorator("checkInDate", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input check-in date!"
                                        }
                                    ]
                                })(<DatePicker placeholder="Check-In" />)}
                            </FormItem>
                        </Col>
                        <Col span={2}>
                            <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                            -
                            </span>
                        </Col>
                        <Col span={11}>
                            <FormItem>
                                {getFieldDecorator("checkOutDate", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input check-out date!"
                                        }
                                    ]
                                })(<DatePicker placeholder="Check-Out" />)}
                            </FormItem>
                        </Col>
                    </FormItem>
                    <FormItem {...formItemLayout} label="Transport By">
                        {getFieldDecorator("transportBy", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input transport by!"
                                }
                            ]
                        })(
                            <Select placeholder="Please select a transportation">
                                <Option value="Air">Air</Option>
                                <Option value="Sea">Sea</Option>
                                <Option value="Rail">Rail</Option>
                                <Option value="Road">Road</Option>
                            </Select>
                        )}
                    </FormItem>
                    <br />
                    <Divider />
                    <h3>Stock Details</h3>
                    <br />
                    <FormItem {...formItemLayout} label="Item ID">
                        {getFieldDecorator("itemID", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input item id!"
                                }
                            ]
                        })(<Input />)}
                    </FormItem> 
                    <FormItem {...formItemLayout} label="Item Name">
                        {getFieldDecorator("itemName", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input item name!"
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                    <FormItem 
                        {...formItemLayout} 
                        label={(
                            <span>
                                Size&nbsp;
                                <Tooltip title="length x width x height (cm.)">
                                    <Icon type="question-circle-o" />
                                </Tooltip>
                            </span>
                        )}
                    >
                        <Col span={6}>
                            <FormItem>
                                {getFieldDecorator("length", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input item length!"
                                        }
                                    ]
                                })(<Input placeholder="L.." />)}
                            </FormItem>
                        </Col>
                        <Col span={3} >
                            <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                x
                            </span>
                        </Col>
                        <Col span={6}>
                            <FormItem>
                                {getFieldDecorator("width", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input item width!"
                                        }
                                    ]
                                })(<Input placeholder="W.." />)}
                            </FormItem>
                        </Col>
                        <Col span={3} >
                            <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                x
                            </span>
                        </Col>
                        <Col span={6}>
                            <FormItem>
                                {getFieldDecorator("height", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input item height!"
                                        }
                                    ]
                                })(<Input placeholder="H.." />)}
                            </FormItem>
                        </Col>
                    </FormItem>
                    <FormItem {...formItemLayout} label="Weight">
                        {getFieldDecorator("weight", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input item weight!"
                                }
                            ]
                        })(<Input addonAfter="Kg."/>)}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Quantity/Unit">
                        {getFieldDecorator("unit", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please input unit!"
                                }
                            ]
                        })(<InputNumber />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Item Type">
                        {getFieldDecorator("itemType", {
                            rules: [
                                {
                                    required: true,
                                    message: "Please select item type!"
                                }
                            ]
                        })(
                            <Select placeholder="Please select item type">
                                <Option value="General">General</Option>
                                <Option value="Temperature-Controlled">Temperature-Controlled</Option>
                                <Option value="Dangerous">Dangerous</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Description">
                        {getFieldDecorator("description", {
                        })(<TextArea autosize />)}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </FormItem>
                    <br />
                </Form>
                )}
            </div>
        );
    }
}

const WrappedAddStockFormForm = Form.create()(AddStockForm);

export default connect(null, { suggestInventoryLocation })(WrappedAddStockFormForm);
