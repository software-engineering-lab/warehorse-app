import React, { Component } from "react";
import { connect } from "react-redux";
import { saveStockAndLocation } from "../../actions/Stock";
import { Card, Avatar, Form, Divider, List, Row, Col, Input, Checkbox, Button } from "antd";

const { Meta } = Card;
const FormItem = Form.Item;

class SuggestLocated extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkBoxDisabled: false,
            checkBoxSelect: [],
            stockInfo: {}
        }
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
        this.resetCheckBox = this.resetCheckBox.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        var checkBoxSelectTemp = []
        for (let i = 0; i < this.props.suggestData.suggestion.length; i++) {
            checkBoxSelectTemp[i] = false;
        }
        this.setState({
            checkBoxSelect: checkBoxSelectTemp
        })
    }

    checkBoxOnChange(checkBoxNum) {
        var checkBoxSelectTemp = this.state.checkBoxSelect;
        checkBoxSelectTemp[checkBoxNum - 1] = true;
        this.setState({ 
            checkBoxDisabled: !this.state.checkBoxDisabled,
            checkBoxSelect: checkBoxSelectTemp
        })
    }

    resetCheckBox() {
        var checkBoxSelectTemp = []
        for (let i = 0; i < this.props.suggestData.suggestion.length; i++) {
            checkBoxSelectTemp[i] = false;
        }
        this.setState({
            checkBoxDisabled: false,
            checkBoxSelect: checkBoxSelectTemp
        })
    }

    handleSubmit(event) {
        event.preventDefault();  
        let selectIndex; 
        for (let i = 0; i < this.state.checkBoxSelect.length; i++) {
            if (this.state.checkBoxSelect[i] === true) {
                selectIndex = i;
                break;
            }
        }
        const location = this.props.suggestData.suggestion[selectIndex];
        var saveStock = {
            _id: this.props.formData.itemID, 
            ...this.props.formData,
            location: location,
        }
        this.props.saveStockAndLocation(saveStock).then(res => {
            if (res) {
                window.location.reload();
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 2 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 }
            }
        };
        const formStockLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 }
            }
        };
        return (
            <Card>
                <Meta
                    avatar={<Avatar>{this.props.suggestData.itemName[0]}</Avatar>}
                    title={this.props.suggestData.itemName}
                    description={this.props.suggestData.itemID}
                />
                <Divider />
                <Form className="suggestion-form">
                    <FormItem {...formItemLayout} label="Item Type">
                       {this.props.suggestData.itemType}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Item Status">
                        "Prepare to In-Stocked"
                    </FormItem>
                    <br />
                    <List
                        header={<div>Stock Location (Please Select Only One)</div>}
                        bordered
                        dataSource={this.props.suggestData.suggestion}
                        renderItem={item => (
                            <List.Item>
                                <Row gutter={16}>
                                    <Col span={6} >
                                        <FormItem {...formStockLayout} label="Building">
                                            {getFieldDecorator("building", {
                                                initialValue: `${item.building}`
                                            })(
                                                <Input disabled/>
                                                )}
                                        </FormItem>
                                    </Col>
                                    <Col span={6} >
                                        <FormItem {...formStockLayout} label="Section">
                                            {getFieldDecorator("section", {
                                                initialValue: `${item.section}`
                                            })(
                                                <Input disabled/>
                                                )}
                                        </FormItem>
                                    </Col>
                                    <Col span={6}>
                                        <FormItem {...formStockLayout} label="Level">
                                            {getFieldDecorator("level", {
                                                initialValue: `${item.level}`
                                            })(
                                                <Input disabled/>
                                            )}
                                        </FormItem>
                                    </Col>
                                    <Col span={6}>
                                        <FormItem {...formStockLayout} label="Bay">
                                            {getFieldDecorator("bay", {
                                                initialValue: `${item.bay}`
                                            })(
                                                <Input disabled/>
                                            )}
                                        </FormItem>
                                    </Col>
                                    <Col span={6}>
                                        <FormItem {...formStockLayout} label="Shelf">
                                            {getFieldDecorator("shelf", {
                                                initialValue: `${item.shelf}`
                                            })(
                                                <Input disabled/>
                                            )}
                                        </FormItem>
                                    </Col>                                 
                                </Row>
                                <FormItem style={{marginLeft: "2em"}} >
                                    {getFieldDecorator(`${item.no}`, {
                                    })(                                      
                                        <Checkbox   
                                            checked={this.state.checkBoxSelect[item.no - 1]}
                                            disabled={this.state.checkBoxDisabled}
                                            onChange={() => this.checkBoxOnChange(item.no)}
                                        >
                                        </Checkbox>
                                    )}
                                </FormItem>
                            </List.Item>
                        )}
                    />
                    <br />
                    <Button
                        onClick={this.resetCheckBox}
                    >
                        Select Again
                    </Button>
                    <Button
                        type="primary"
                        onClick={this.handleSubmit}
                        style={{ marginLeft: "1em" }}
                    >
                        Submit
                    </Button>
                </Form>
            </Card>
        );
    }
}

const WrappedSuggestLocatedForm = Form.create()(SuggestLocated);


export default connect(null, { saveStockAndLocation })(WrappedSuggestLocatedForm);