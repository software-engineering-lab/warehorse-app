import React, { Component } from "react"
import { connect } from "react-redux";
import { getAllStock } from "../../actions/Stock";
import { addItemStatus } from "../../utils/itemStatus";
import { List, Avatar, Modal } from "antd";

class StockList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stock: [],
            visible: false,
            modalData: {}
        }
        this.showModal = this.showModal.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleOk = this.handleOk.bind(this);
    }

    componentDidMount() {
        this.props.getAllStock().then(res => {
            this.setState({
                stock: res.data,
            });
        });
    }

    showModal(data) {
        this.setState({ 
            visible: true,
            modalData: data,
        });
    }

    handleCancel() {
        this.setState({ visible: false });
    }

    handleOk() {
        this.setState({ visible: false });
    }


    render() {
        const stockData = this.state.stock;
        const modalData = this.state.modalData;
        const location = modalData.location;
        return (
            <div>            
                <Modal
                    title={
                        <div>
                            {modalData.itemID}&nbsp;
                            <b>{modalData.itemName}</b>
                        </div>
                    }
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <div>
                        <p>Type: <b>{modalData.itemType}</b> </p>
                        <p>Seller: <b>{modalData.seller}</b> </p>
                        <p>Buyer: <b>{modalData.buyer}</b> </p>
                        <p>Check-In: <b>{modalData.checkInDate}</b> </p>
                        <p>Check-Out: <b>{modalData.checkOutDate}</b> </p>
                        <p>Transport: <b>{modalData.transportBy}</b> </p>
                        <p>Size: <b>{modalData.width}(W) x {modalData.length}(L) x {modalData.height}(H)</b> cm.</p>
                        <p>Weight: <b>{modalData.weight}</b> Kg. </p>
                        <p>Quantity/Unit: <b>{modalData.unit}</b> </p>
                        <p>Status: <b>{modalData.status}</b> </p>
                        { location ? (
                            <p>
                                Location: <b>{location._id}</b>,&nbsp; 
                                Building: <b>{location.building}</b>,&nbsp; 
                                Section: <b>{location.section}</b>,&nbsp;
                                Level: <b>{location.level}</b>,&nbsp;
                                Bay: <b>{location.bay}</b>,&nbsp;
                                Shelf: <b>{location.shelf}</b>&nbsp;
                            </p>
                        ) : ("") }
                        { modalData.description ? (
                            <p>
                                Description: <b>{modalData.description}</b>,&nbsp; 
                            </p>
                        ) : ("") }
                    </div>
                </Modal>
                <List
                    itemLayout="horizontal"
                    dataSource={stockData}
                    renderItem={item => (
                        <List.Item actions={
                            [
                                <div>{addItemStatus(item.status)}</div>,
                                <div onClick={() => this.showModal(item)}>more</div>,
                            ]
                        }>
                            <List.Item.Meta
                                avatar={
                                    <Avatar>{item.itemName[0]}</Avatar>
                                }
                                title={
                                    <div onClick={() => this.showModal(item)}> 
                                        {item.itemID}, <b>{item.itemName}</b>
                                    </div>
                                }
                                description={
                                    <div>
                                        Type: <b>{item.itemType}</b>,&nbsp;
                                        Seller: <b>{item.seller}</b>,&nbsp;
                                        Buyer: <b>{item.buyer}</b>,&nbsp;
                                        Location: <b>{item.location._id}</b>,&nbsp;
                                    </div>
                                }
                            />
                        </List.Item>
                    )}
                />
            </div>
        );
    }

}

export default connect(null, { getAllStock })(StockList);