import React from "react";
import { Link } from "react-router-dom";
import { List, Icon, Card } from "antd";

const data = [
    {
        title: "Add Stock",
        icon: <Icon type="plus" />,
        url: "/add-stock"
    },
    {
        title: "See All Stocks",
        icon: <Icon type="search" />,
        url: "/stocks"
    },
    {
        title: "Inventory",
        icon: <Icon type="hdd" />,
        url: "/inventory"
    }
];

export default function HomeContent(props) {
    return (
        <div>
            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource={data}
                renderItem={item => (
                    <List.Item>
                        <Link to={item.url} style={{ textDecoration: "none" }}>
                            <Card 
                                hoverable
                            >
                                {item.icon}&nbsp;&nbsp;&nbsp;
                                {item.title}
                            </Card>
                        </Link>
                    </List.Item>
                )}
            />
        </div>
    );
}
