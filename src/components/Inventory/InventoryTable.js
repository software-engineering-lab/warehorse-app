import React from "react";
import { connect } from "react-redux";
import { getAllInventory } from "../../actions/Inventory";
import { addKeyInventoryTable } from "../../utils/addKeyInventoryTable";
import { Table } from "antd";

const columns = [
    {
        title: "Building",
        dataIndex: "building",
        key: "building"
    },
    {
        title: "Section",
        dataIndex: "section",
        key: "section"
    },
    {
        title: "Level",
        dataIndex: "level",
        key: "level",
    },
    {
        title: "Bay",
        key: "bay",
        dataIndex: "bay"
    },
    {
        title: "Shelf",
        key: "shelf",
        dataIndex: "shelf"
    },
    {
        title: "Status",
        key: "status",
        dataIndex: "status"
    },    
    {
        title: "Stock ID",
        key: "stockId",
        dataIndex: "stockId"
    }
];

class InventoryTable extends React.Component {

    constructor() {
        super();
        this.state = {
            allInventory: [],
        }
    }

    componentDidMount() {
        this.props.getAllInventory().then(res => {
            const addKey = addKeyInventoryTable(res.data);
            this.setState({
                allInventory: addKey
            })
        })
    }

    render() {
        return (
            <Table columns={columns} dataSource={this.state.allInventory} size="middle" />
        );
    }
}

export default connect(null, { getAllInventory })(InventoryTable);
