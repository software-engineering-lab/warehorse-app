import { combineReducers } from "redux";
import AppBarToggle from "./AppBarToggle";
import PageName from "./PageNameUpdate";

export default combineReducers({
    AppBarToggle,
    PageName
});
