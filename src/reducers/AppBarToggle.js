export default function (state = 0, action) {
    switch (action.type) {
        case "TOGGLE_MENU_BAR":
            return action.status;
        default:
            return state;
    }
};
