export default function (state = 0, action) {
    switch (action.type) {
        case "UPDATE_PAGE_NAME":
            return action.pageName;
        default:
            return state;
    }
};
