import React, { Component } from "react";
import { connect } from "react-redux";
import AddStockForm from "../../components/AddStock/";
import AppMenu from "../../components/MenuBar/";
import AppBar from "../../components/AppBar";
import { updatePageName } from "../../actions/PageNameAction";
import { Layout } from "antd";

const { Content } = Layout;

class AddStock extends Component {
    componentWillMount() {
        this.props.dispatch(updatePageName("Add Stock"));
    }

    render() {
        return (
            <Layout style={{ minHeight: "100vh" }}>
                <AppMenu />
                <Layout>
                    <AppBar />
                    <Content
                        style={{
                            margin: "3em",
                            padding: "3em",
                            background: "#fff",
                            minHeight: 500
                        }}
                    >
                        <AddStockForm />
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default connect()(AddStock);