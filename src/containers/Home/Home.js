import React, { Component } from "react";
import { connect } from "react-redux";
import AppMenu from "../../components/MenuBar/";
import AppBar from "../../components/AppBar/";
import HomeContent from "../../components/HomeContent/";
import { updatePageName } from "../../actions/PageNameAction";
import { Layout } from "antd";

const { Content } = Layout;

class Home extends Component {

    componentWillMount() {
        this.props.dispatch(updatePageName("Warehorse"));
    }

    render() {
        return (
            <Layout style={{ minHeight: "100vh" }}>
                <AppMenu />
                <Layout>
                    <AppBar />
                    <Content
                        style={{
                            margin: "3em",
                            padding: "3em",
                            background: "#fff",
                            minHeight: 500
                            
                        }}
                    >
                        <HomeContent />
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default connect()(Home);
