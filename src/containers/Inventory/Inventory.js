import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux'
import AppMenu from "../../components/MenuBar/";
import AppBar from "../../components/AppBar/";
import InventoryTable from "../../components/Inventory/InventoryTable";
import { updatePageName } from "../../actions/PageNameAction";
import { addNewInventory } from "../../actions/Inventory";
import { Layout, Form, Col, Icon, Button, Tooltip, Input, Modal, InputNumber, Radio } from "antd";

const { Content } = Layout;

const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

class Inventory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        }
        this.showModal = this.showModal.bind(this);
        this.handleModalOk = this.handleModalOk.bind(this);
        this.handleModalCancel = this.handleModalCancel.bind(this);
    }

    showModal() {
        this.setState({
            modalVisible: true,
        })
    }

    handleModalOk(event) {
        this.setState({
            modalVisible: false,
        })
        event.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.props.addNewInventory(values).then(res => {
                    window.location.reload();
                })
            }
        });
    }

    handleModalCancel() {
        this.setState({
            modalVisible: false,
        })     
    }

    componentWillMount() {
        this.props.dispatch(updatePageName("Inventory"));
    }

    render() {

        const { getFieldDecorator } = this.props.form;
        return (
            <Layout style={{ minHeight: "100vh" }}>
                <AppMenu />
                <Layout>
                    <AppBar />
                    <Content
                        style={{
                            margin: "3em",
                            padding: "3em",
                            background: "#fff",
                            minHeight: 500
                        }}
                    >
                        <Button type="primary" onClick={this.showModal}>Add New Inventory</Button>
                        <Modal
                            title="Add New Inventory"
                            visible={this.state.modalVisible}
                            onOk={this.handleModalOk}
                            onCancel={this.handleModalCancel}
                        >
                            <Form>
                                <Form.Item
                                    label="Building"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator("building", {
                                    })(
                                        <Input />
                                    )}
                                </Form.Item>
                                <Form.Item {...formItemLayout} label="For Type">
                                    {getFieldDecorator("type", {
                                        initialValue: "general"
                                    })(
                                        <Radio.Group buttonStyle="solid">
                                            <Radio.Button value="General">
                                                General
                                            </Radio.Button>
                                            <Radio.Button value="Temperature-Controlled">
                                                Temperature-Controlled
                                            </Radio.Button>
                                            <Radio.Button value="Dangerous">
                                                Dangerous
                                            </Radio.Button>
                                        </Radio.Group>
                                    )}
                                </Form.Item>
                                <Form.Item
                                    label={(
                                    <span>
                                        Sections&nbsp;
                                        <Tooltip title="Single Capital letter only! e.g. A">
                                            <Icon type="question-circle-o" />
                                        </Tooltip>
                                    </span>
                                    )}
                                    {...formItemLayout}
                                >
                                    <Col span={11}>
                                        <Form.Item>
                                            {getFieldDecorator("startSection", {
                                            })(
                                                <Input placeholder="Start" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={2}>
                                        <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                        -
                                        </span>
                                    </Col>
                                    <Col span={11}>
                                        <Form.Item>
                                            {getFieldDecorator("endSection", {
                                            })(
                                                <Input placeholder="End" />
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Form.Item>
                                <Form.Item
                                    label="Levels"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator("levels", {
                                    })(
                                        <InputNumber />
                                    )}
                                </Form.Item>
                                <Form.Item
                                    label="Bays"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator("bays", {
                                    })(
                                        <InputNumber />
                                    )}
                                </Form.Item>
                                <Form.Item
                                    label="Shelfs"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator("shelfs", {
                                    })(
                                        <InputNumber />
                                    )}
                                </Form.Item>
                            </Form>
                        </Modal>
                        <br /><br />
                        <InventoryTable />
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

const WrappedInventoryForm = Form.create()(Inventory);

function mapDispatchToProps(dispatch) { 
    return { 
        dispatch, 
        addNewInventory: bindActionCreators(addNewInventory, dispatch),
    } 
}

Inventory.propTypes = {
    getAllInventory: PropTypes.func
};

export default connect(null, mapDispatchToProps)(WrappedInventoryForm);