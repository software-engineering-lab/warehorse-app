import React, { Component } from "react";
import { connect } from "react-redux";
import AppMenu from "../../components/MenuBar/";
import AppBar from "../../components/AppBar/";
import StockList from "../../components/StockDetails";
import { updatePageName } from "../../actions/PageNameAction";
import { Layout, Menu, Icon, Button, Dropdown, Input } from "antd";

const { Content } = Layout;
const Search = Input.Search;

const typeMenu = (
    <Menu>
        <Menu.Item key="1">
            General
        </Menu.Item>
        <Menu.Item key="2">
            Temperature-Controlled
        </Menu.Item>
        <Menu.Item key="3">
            Dangerous
        </Menu.Item>
    </Menu>
);

const statusMenu = (
    <Menu>
        <Menu.Item key="1">
            In-Stock
        </Menu.Item>
        <Menu.Item key="2">
            Ready to Ship
        </Menu.Item>
        <Menu.Item key="3">
            Shipped
        </Menu.Item>
    </Menu>
);

class Stock extends Component {
    componentWillMount() {
        this.props.dispatch(updatePageName("Stocks"));
    }

    render() {
        return (
            <Layout style={{ minHeight: "100vh" }}>
                <AppMenu />
                <Layout>
                    <AppBar />
                    <Content
                        style={{
                            margin: "3em",
                            padding: "3em",
                            background: "#fff",
                            minHeight: 500
                        }}
                    >
                        <Dropdown overlay={typeMenu}>
                            <Button style={{ marginLeft: 8 }}>
                                Types <Icon type="down" />
                            </Button>
                        </Dropdown>
                        <Dropdown overlay={statusMenu}>
                            <Button style={{ marginLeft: 8 }}>
                                Status <Icon type="down" />
                            </Button>
                        </Dropdown>
                        <Search
                            placeholder="search..."
                            onSearch={value => console.log(value)}
                            enterButton
                            style={{ width: 250, marginLeft: "1em" }}
                        />
                        <br /><br />
                        <StockList />
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default connect()(Stock);
