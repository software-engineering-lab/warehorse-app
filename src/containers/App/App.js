import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AddStock from "../AddStock/AddStock";
import Stock from "../Stock/Stock";
import Login from "../Login/Login";
import Home from "../Home/Home";
import Inventory from "../Inventory/Inventory";
import "./App.css";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Switch>
                        <Route path="/" component={Home} exact={true} />
                        <Route path="/login" component={Login} />
                        <Route path="/add-stock" component={AddStock} />
                        <Route path="/stocks" component={Stock} />
                        <Route path="/inventory" component={Inventory} />
                        <Route component={Error} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
