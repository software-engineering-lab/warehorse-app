import React, { Component } from "react";
import LoginForm from "../../components/Login/";
import "./Login.css";

class Login extends Component {
    render() {
        return (
            <div className="login-content">
                <LoginForm />
            </div>
        );
    }
}

export default Login;
    